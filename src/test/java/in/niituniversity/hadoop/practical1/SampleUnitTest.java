package in.niituniversity.hadoop.practical1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SampleUnitTest {
    @Test
    void sampleUnitTest() {
        FileIO fileIO = new FileIO();
        assertTrue(fileIO.sampleUnitTest(), "Invoking Sample Unit Test");
    }
}
