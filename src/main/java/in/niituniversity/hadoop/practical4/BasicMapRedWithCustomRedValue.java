package in.niituniversity.hadoop.practical4;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class BasicMapRedWithCustomRedValue extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int exit = ToolRunner.run(new BasicMapRedWithCustomRedValue(), args);
        System.exit(exit);
    }

    @Override
    public int run(String[] args) throws Exception {
        Job jc = Job.getInstance(getConf());
        jc.setJarByClass(BasicMapRedWithCustomRedValue.class);

        jc.setNumReduceTasks(0);
        jc.setOutputKeyClass(LongWritable.class);
        jc.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(jc, new Path(args[0]));
        FileOutputFormat.setOutputPath(jc, new Path(args[1]));

        boolean s = jc.waitForCompletion(true);

        return s ? 0 : 1;
    }
}
