package in.niituniversity.hadoop.practical1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

import java.io.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class FileIO {

    public static void main(String[] args) throws IOException {
        String mode = null;
        String uri = null;
        String dest = null;

        if (args.length == 3) {
            mode = args[0];
            uri = args[1];
            dest = args[2];
        } else if (args.length == 2) {
            mode = args[0];
            uri = args[1];
        } else if (args.length == 1) {
            mode = args[0];
        } else {
            System.out.println("Invalid number of input arguments.");
            System.out.println("Usage : hadoop jar " + System.class.getName() + ".jar help");
            System.exit(1);
        }

        if (mode.equalsIgnoreCase("read") || mode.equalsIgnoreCase("r")) { // Start execution in read mode
            System.out.println(">>>Executing Problem Statement 1: Read given file from HDFS");
            problemOne(uri);

            System.out.println("\n\n>>>Executing Problem Statement 2: Read the same file twice from HDFS");
            problemTwo(uri);

            System.out.println("\n\n>>>Executing Problem Statement 3: Read from offset position 5 to 10");
            problemThree(uri);

            System.out.println("\n\n>>>Executing Problem Statement 4: Count the occurrence of every word in the file");
            problemFour(uri);
        } else if (mode.equalsIgnoreCase("write") || mode.equalsIgnoreCase("w")) { // Start execution in write mode
            System.out.println("\n\n>>>Executing Problem Statement 5: Write a file to HDFS");
            problemFive(uri, dest);
        } else if (mode.equalsIgnoreCase("help") || mode.equalsIgnoreCase("h")) { // Print the help utility
            printHelp();
        } else {
            System.out.println("Invalid input arguments.");
            System.out.println("Usage : hadoop jar " + System.class.getName() + ".jar help");
            System.exit(1);
        }
    }

    public static void problemOne(String uri) throws IOException {
        Configuration con = new Configuration(); //core-site.xml
        FileSystem fs = FileSystem.get(URI.create(uri), con);
        FSDataInputStream in = null;
        try {
            in = fs.open(new Path(uri));
            IOUtils.copyBytes(in, System.out, 4096, false);
        } finally {
            IOUtils.closeStream(in);
            in.close();
            fs.close();
        }
    }

    public static void problemTwo(String uri) throws IOException {
        Configuration con = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(uri), con);
        FSDataInputStream in = null;
        try {
            // Reading for the first time
            in = fs.open(new Path(uri));
            IOUtils.copyBytes(in, System.out, 4096, false);
            System.out.println("------------------------");

            // Reading for the second time
            in.seek(0);
            IOUtils.copyBytes(in, System.out, 4096, false);
        } finally {
            IOUtils.closeStream(in);
            in.close();
            fs.close();
        }
    }

    public static void problemThree(String uri) throws IOException {
        Configuration con = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(uri), con);
        FSDataInputStream in = null;
        try {
            in = fs.open(new Path(uri));
            int length = in.available();
            byte[] buffer = new byte[length];
            in.read(4, buffer, 0, 5);
            System.out.println(new String(buffer));
        } finally {
            in.close();
            fs.close();
        }
    }

    public static void problemFour(String uri) throws IOException {
        Configuration con = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(uri), con);
        FSDataInputStream in = null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            in = fs.open(new Path(uri));
            IOUtils.copyBytes(in, byteArrayOutputStream, 4096, false);
            String[] arr = byteArrayOutputStream.toString().trim().replaceAll("[^0-9a-zA-Z *]", "").split(" ");
            Map<String, Integer> map = new HashMap<>();
            for (String s : arr) {
                if (map.containsKey(s)) {
                    map.put(s, map.get(s) + 1);
                } else {
                    map.put(s, 1);
                }
            }

            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                System.out.println(entry.getKey() + " - " + entry.getValue());
            }
        } finally {
            IOUtils.closeStream(in);
            in.close();
            fs.close();
        }
    }

    public static void problemFive(String source, String destination) throws IOException {
        Configuration con = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(destination), con);

        // Checking if file already exists in the HDFS
        if (fs.exists(new Path(destination))) {
            System.out.println("File " + destination + " already exists!");
            return;
        }

        FSDataOutputStream out = fs.create(new Path(destination));
        InputStream in = new BufferedInputStream(new FileInputStream(source));

        try {
            byte[] b = new byte[1024];
            int numBytes = 0;
            while ((numBytes = in.read(b)) > 0) {
                out.write(b, 0, numBytes);
            }
        } finally {
            in.close();
            out.close();
            fs.close();
        }
    }

    public static void printHelp() {
        System.out.println("This jar accepts below arguments to process the file in hadoop:");

        System.out.println("\n1. Read Mode: Only for read operation from HDFS. Two input arguments are required");
        System.out.println("Command can be executed as - hadoop jar pathOfJar.jar read OR r /filePathInsideHDFS");
        System.out.println("e.g. hadoop jar FileIO.jar read /sampleFile.txt");

        System.out.println("\n2. Write Mode: Only for write operation in HDFS. Three input arguments are required");
        System.out.println("Command can be executed as - hadoop jar pathOfJar.jar write OR w /sourceFilePath /destinationFilePath");
        System.out.println("e.g. hadoop jar FileIO.jar write /home/hdoop/some/path/sampleFile.txt /file/sampleFile.txt");

        System.out.println("\n3. Help: Prints this little help utility");
        System.out.println("Command can be executed as - hadoop jar pathOfJar.jar help OR h");
        System.out.println("e.g. hadoop jar FileIO.jar h");

        System.out.println("\nSome other useful hadoop commands are listed below:");
        System.out.println("Command to list files in HDFS : hdfs dfs –ls /");
        System.out.println("Command to make a directory in HDFS : hdfs dfs -mkdir <hdfs path>");
        System.out.println("Command to copy a file in HDFS : hdfs dfs –put /source_file_path /dest_file_path");
        System.out.println("Command to remove files from HDFS : hdfs dfs –rm <hdfs_path>");
        System.out.println("Command to execute the generated JAR in Hadoop : hadoop jar pathOfJarFile.jar inputArg[0] inputArg[1] ...");
    }

    public boolean sampleUnitTest() {
        System.out.println("Sample Unit Test is executed");
        return true;
    }
}