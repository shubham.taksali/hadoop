package in.niituniversity.hadoop.practical3.problem2;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class DistinctTempReducer extends Reducer<FloatWritable, NullWritable, FloatWritable, NullWritable> {
    @Override
    public void reduce(FloatWritable temperature, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
        context.write(temperature, NullWritable.get());
    }
}
