package in.niituniversity.hadoop.practical3.problem2;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class DistinctTemp extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int exit = ToolRunner.run(new DistinctTemp(), args);
        System.exit(exit);
    }

    @Override
    public int run(String[] args) throws Exception {
        Job jc = Job.getInstance(getConf());
        jc.setJarByClass(DistinctTemp.class);

        jc.setMapperClass(DistinctTempMapper.class);
        jc.setReducerClass(DistinctTempReducer.class);

        jc.setMapOutputKeyClass(FloatWritable.class);
        jc.setMapOutputValueClass(NullWritable.class);
        jc.setOutputKeyClass(FloatWritable.class);
        jc.setOutputValueClass(NullWritable.class);

        FileInputFormat.addInputPath(jc, new Path(args[0]));
        FileOutputFormat.setOutputPath(jc, new Path(args[1]));

        boolean s = jc.waitForCompletion(true);

        return s ? 0 : 1;
    }
}
