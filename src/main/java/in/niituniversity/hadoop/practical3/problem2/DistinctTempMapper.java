package in.niituniversity.hadoop.practical3.problem2;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class DistinctTempMapper extends Mapper<LongWritable, Text, FloatWritable, NullWritable> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String line = value.toString();
        float temperature = Float.parseFloat(line.substring(39, 45).trim());

        context.write(new FloatWritable(temperature), NullWritable.get());
    }
}
