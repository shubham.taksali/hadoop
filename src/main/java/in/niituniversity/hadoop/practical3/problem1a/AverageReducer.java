package in.niituniversity.hadoop.practical3.problem1a;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class AverageReducer extends Reducer<Text, FloatWritable, Text, FloatWritable> {

    @Override
    public void reduce(Text month, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException {

        float tmp = 0, sum = 0;
        int size = 0;
        for (FloatWritable v : values) {
            tmp = v.get();
            sum = sum + tmp;
            size++;
        }
        float avg = sum / size;

        context.write(month, new FloatWritable(avg));
    }
}
