package in.niituniversity.hadoop.practical3.problem3;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class DistinctMonthEntriesReducer extends Reducer<Text, FloatWritable, Text, IntWritable> {

    @Override
    protected void reduce(Text month, Iterable<FloatWritable> values, Reducer<Text, FloatWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        Set<Float> set = new HashSet<>();
        for (FloatWritable v : values) {
            set.add(v.get());
        }

        context.write(month, new IntWritable(set.size()));
    }
}
