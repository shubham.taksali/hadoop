package in.niituniversity.hadoop.practical3.problem3;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class DistinctMonthEntriesMapper extends Mapper<LongWritable, Text, Text, FloatWritable> {

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, FloatWritable>.Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String month = line.substring(6, 12).trim();
        float temperature = Float.parseFloat(line.substring(39, 45).trim());

        context.write(new Text(month), new FloatWritable(temperature));
    }
}
