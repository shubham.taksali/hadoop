package in.niituniversity.hadoop.practical3.problem1b;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MaxTempMapper extends Mapper<LongWritable, Text, Text, FloatWritable> {

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String line = value.toString();
        String month = line.substring(6, 12).trim();
        float temperature = Float.parseFloat(line.substring(39, 45).trim());

        context.write(new Text(month), new FloatWritable(temperature));
    }

}