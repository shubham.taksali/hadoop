package in.niituniversity.hadoop.practical3.problem1b;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MaxTemp extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int exit = ToolRunner.run(new MaxTemp(), args);
        System.exit(exit);
    }

    @Override
    public int run(String[] args) throws Exception {
        Job jc = Job.getInstance(getConf());
        jc.setJarByClass(MaxTemp.class);

        jc.setMapperClass(MaxTempMapper.class);
        jc.setCombinerClass(MaxTempReducer.class);
        jc.setReducerClass(MaxTempReducer.class);

        jc.setMapOutputKeyClass(Text.class);
        jc.setMapOutputValueClass(FloatWritable.class);
        jc.setOutputKeyClass(Text.class);
        jc.setOutputValueClass(FloatWritable.class);

        FileInputFormat.addInputPath(jc, new Path(args[0]));
        FileOutputFormat.setOutputPath(jc, new Path(args[1]));

        boolean s = jc.waitForCompletion(true);

        return s ? 0 : 1;

    }
}
