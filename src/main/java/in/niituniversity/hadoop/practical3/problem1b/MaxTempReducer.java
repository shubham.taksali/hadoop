package in.niituniversity.hadoop.practical3.problem1b;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MaxTempReducer extends Reducer<Text, FloatWritable, Text, FloatWritable> {

    @Override
    public void reduce(Text month, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException {

        float max = Float.MIN_VALUE;
        for (FloatWritable v : values) {
            if (max < v.get()) {
                max = v.get();
            }
        }

        context.write(month, new FloatWritable(max));
    }
}
