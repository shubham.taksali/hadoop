A = load '/home/ubuntu/PIG/sample.txt' using PigStorage(',') AS (cid:charArray,pid:charArray,qty:int);
B = FILTER A BY qty>20;
pcid = FOREACH A GENERATE cid,$1,$2,'sample.txt' AS lineage;
filterB = FILTER A BY $2 > 0;
groupPdt = GROUP filterB BY pid;
result = FOREACH groupPdt GENERATE group AS pid, SUM(filterB.qty) AS totalqty;
STORE result into '/home/ubuntu/PIG/sumByPdt';
