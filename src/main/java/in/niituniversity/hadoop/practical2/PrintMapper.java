package in.niituniversity.hadoop.practical2;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class PrintMapper extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String date = line.substring(6, 14).trim();
        float temp = Float.parseFloat(line.substring(39, 45).trim());

        if (temp >= 0.0) {
            context.write(new Text(date), new Text(String.valueOf(temp).concat(" - Cold Day")));
        }
        if (temp <= 3.0) {
            context.write(new Text(date), new Text(String.valueOf(temp).concat(" - Freezing Day")));
        }
    }
}
